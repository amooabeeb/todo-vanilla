"use strict";
var todos = [];

function addTodo() {
    var input = document.getElementById("todo_input");
    var todo = new Todo(input.value);
    todos.push(todo);
    input.value = '';
    renderTodos();
}

function renderTodos() {
    var renderer = document.getElementById("todos");
    var template = "<ul>";
    for (var i = 0; i < todos.length; i++ ) {
        var todo = todos[i];
        template += "<li>" + todo.name + "</li>";
    }
    template += "</ul>";
    renderer.innerHTML = template;
}